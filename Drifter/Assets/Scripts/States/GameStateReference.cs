﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CurrentGameState", menuName = "States/CurrentGameState")]
public class GameStateReference : ScriptableObject
{
	public GameState currentState;
}