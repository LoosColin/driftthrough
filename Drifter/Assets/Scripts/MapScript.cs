﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScript : MonoBehaviour
{
	[Header("Prefab Links")]
	[SerializeField]
	private GameObject carPrefab;

	[SerializeField]
	private GameObject spawnPoint;

	[SerializeField]
	private GameObject finish;

	private GameObject car = null;

	private void Start ()
	{
		if (carPrefab == null)
		{
			Debug.LogError("Car prefab is not linked!");
		}

		if(spawnPoint == null)
		{
			Debug.LogError("Spawnpoint not linked!");
		}

		if(finish == null)
		{
			Debug.LogError("Finish not linked!");
		}

		SpawnCar();

		InitializeCamera();

		this.gameObject.GetComponent<GameplayScript>().SetCar(car);
	}
	
	private void Update ()
	{
		
	}

	public void OnRetry()
	{
		ResetCar();
	}

	private void SpawnCar()
	{
		car = (GameObject)Instantiate(carPrefab);
		car.transform.position = spawnPoint.transform.position;
		car.transform.rotation = spawnPoint.transform.rotation;
	}

	private void ResetCar()
	{
		car.GetComponent<Rigidbody2D>().angularVelocity = 0f;
		car.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

		car.transform.position = spawnPoint.transform.position;
		car.transform.rotation = spawnPoint.transform.rotation;

		car.GetComponent<CarController>().enabled = false;
	}

	private void InitializeCamera()
	{
		Camera cam = Camera.main;
		// cam.orthographicSize = 4;
		// cam.backgroundColor = new Color(0.3f, 0.6f, 0.3f);

		if (cam.GetComponent<CameraFollow>() == null)
		{
			cam.gameObject.AddComponent<CameraFollow>();
			cam.GetComponent<CameraFollow>().SetTarget(car);
		}
		else
		{
			if (cam.GetComponent<CameraFollow>().GetTarget() == null)
			{
				cam.GetComponent<CameraFollow>().SetTarget(car);
			}
		}

		cam.GetComponent<CameraFollow>().enabled = true;
	}
}
