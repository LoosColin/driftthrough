﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameplayState
{
	Ready,
	Driving,
	Finish
}

public class GameplayScript : MonoBehaviour
{
	[Header("Links")]
	[SerializeField]
	private GameObject spawnpoint;

	[SerializeField]
	private GameObject finish;

	private GameObject car;
	private GameplayState gameplayState;
	private GameScript gameScript;
	private float hitCooldown;
	private float hitElapsedTime;
	private float elapsedTime;
	private float elapsedStateTime;
	private short hits;

	private void Start ()
	{
		hitCooldown = 0.5f;
		hitElapsedTime = 0f;
		elapsedStateTime = 0f;

		if (finish == null)
		{
			Debug.LogError("No finish gameObject linked!");
		}
		else
		{
			if(finish.GetComponent<CircleCollider2D>() == null)
			{
				Debug.LogError("No CircleCollider2D on finish gameObject!");
			}
		}

		gameScript = FindObjectOfType<GameScript>();

		gameplayState = GameplayState.Ready;
	}
	
	private void Update ()
	{
		elapsedStateTime += Time.deltaTime;

		if(gameplayState == GameplayState.Ready)
		{
			if (elapsedStateTime > 0.5f && Input.anyKey)
			{
				car.GetComponent<CarController>().enabled = true;

				elapsedStateTime = 0f;
				gameplayState = GameplayState.Driving;
			}
		}
		else if (gameplayState == GameplayState.Driving)
		{
			elapsedTime += Time.deltaTime;
			hitElapsedTime += Time.deltaTime;

			if (elapsedStateTime > 0.5f && Input.GetButton("Retry"))
			{
				ResetLevel();
			}
		}
		else if(gameplayState == GameplayState.Finish)
		{
			if(elapsedStateTime > 0.5f && Input.anyKey)
			{
				SceneManager.LoadScene(2);
			}
		}
	}

	public void ResetLevel()
	{
		elapsedTime = 0f;
		hits = 0;
		gameScript.SetTime(elapsedTime);
		gameScript.SetHits(hits);
		gameScript.ToggleRetryButton(true);
		gameScript.ToggleBackButton(true);

		GameObject tireTracks = GameObject.Find("TireTracks");
		if (tireTracks != null)
		{
			foreach (Transform child in tireTracks.transform)
			{
				GameObject.Destroy(child.gameObject);
			}
		}

		LeverScript[] levers = FindObjectsOfType<LeverScript>();

		foreach(LeverScript leverScript in levers)
		{
		}

		

		car.transform.position = spawnpoint.transform.position;
		car.transform.rotation = spawnpoint.transform.rotation;

		elapsedStateTime = 0f;
		gameplayState = GameplayState.Ready;
	}

	public void SetCar(GameObject car)
	{
		this.car = car;
	}

	public void Finish()
	{
		if (gameplayState == GameplayState.Driving)
		{
			car.GetComponent<CarController>().enabled = false;

			gameScript.ToggleBackButton(false);
			gameScript.ToggleRetryButton(false);

			elapsedStateTime = 0f;
			this.gameplayState = GameplayState.Finish;
		}
	}

	public void GetHit()
	{
		if (gameplayState == GameplayState.Driving && hitElapsedTime > hitCooldown)
		{
			hits++;
			hitElapsedTime = 0f;
		}
	}
}
