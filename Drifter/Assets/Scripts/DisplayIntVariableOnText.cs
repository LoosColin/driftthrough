﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayIntVariableOnText : MonoBehaviour
{
	[SerializeField]
	private IntReference variable;

	private void Start()
	{
		Display();
	}

	private void Update()
	{
		Display();
	}

	private void Display()
	{
		this.gameObject.GetComponent<Text>().text = variable.value.ToString();
	}
}
