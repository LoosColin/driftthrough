﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayFloatVariableOnText : MonoBehaviour
{
	[SerializeField]
	private FloatReference variable;

	[SerializeField]
	private string format;

	private void Start ()
	{
		Display();
	}
	
	private void Update ()
	{
		Display();
	}

	private void Display()
	{
		this.gameObject.GetComponent<Text>().text = variable.value.ToString(format);
	}
}
