﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Integer", menuName = "Variables/Integer")]
public class IntVariable : ScriptableObject
{
	public int value;
}