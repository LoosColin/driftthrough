﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishSoundScript : MonoBehaviour
{
	private AudioSource audioSource;

	private void Start ()
	{
		this.audioSource = this.GetComponent<AudioSource>();
		
		if (this.audioSource == null)
		{
			Debug.LogError("No audiosource attached to finish!");
		}

		audioSource.playOnAwake = false;
	}
	
	private void Update ()
	{
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			audioSource.Play();
		}
	}
}
