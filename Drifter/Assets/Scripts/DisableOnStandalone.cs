﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnStandalone : MonoBehaviour
{
	public void Start()
	{
#if UNITY_STANDALONE
		this.gameObject.SetActive(false);
#endif
	}
}
