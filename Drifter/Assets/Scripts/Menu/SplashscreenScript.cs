﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashscreenScript : MonoBehaviour
{
	private float elapsedTime;
	
	void Start ()
	{
		
	}
	
	void Update ()
	{
		elapsedTime += Time.deltaTime;

		if(elapsedTime > 3)
		{
			this.LoadMenu();
		}

		if(Input.GetButton("Fire1"))
		{
			this.LoadMenu();
		}
	}

	private void LoadMenu()
	{
		SceneManager.LoadScene(1);
	}
}
