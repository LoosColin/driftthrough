﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectScript : MonoBehaviour
{
	private int selectedLevel;

	private void Start ()
	{
		selectedLevel = -1;
	}
	
	private void Update ()
	{
		
	}

	public void ClickBack()
	{
		SceneManager.LoadScene(1);
	}

	public void ClickPlay()
	{
		SceneManager.LoadScene(3);
		SceneManager.LoadScene(selectedLevel, LoadSceneMode.Additive);
	}

	public void SelectLevel(int index)
	{
		this.selectedLevel = index;
	}
}
