﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverScript : MonoBehaviour
{
	[SerializeField]
	private GameObject door = null;

	[SerializeField]
	private GameEvent onLeverCollisionEvent;

	void Start ()
	{
		
	}
	
	void Update ()
	{
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			door.SetActive(false);
			onLeverCollisionEvent.Raise();
		}
	}

	public void OnRetry()
	{
		door.SetActive(true);
	}
}
