﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScript : MonoBehaviour
{
	[Header("Links")]
	[SerializeField]
	private GameObject textTime;

	[SerializeField]
	private GameObject textHits;

	[SerializeField]
	private GameObject panelFinish;

	[SerializeField]
	private GameObject buttonRetry;

	[SerializeField]
	private GameObject buttonBack;

	private GameplayScript gameplayScript;

	private void Start ()
	{
		gameplayScript = FindObjectOfType<GameplayScript>();
	}
	
	private void Update ()
	{
		
	}

	public void ClickBack()
	{
		SceneManager.LoadScene(2);
	}

	public void ClickRetry()
	{
		gameplayScript.ResetLevel();
	}

	public void SetTime(float time)
	{
		this.textTime.GetComponent<Text>().text = time.ToString();
	}

	public void SetHits(short hits)
	{
		this.textHits.GetComponent<Text>().text = hits.ToString() + " Hits";
	}

	public void ToggleFinishPanel(bool show)
	{
		this.panelFinish.SetActive(show);
	}

	public void ToggleRetryButton(bool show)
	{
		this.buttonRetry.SetActive(show);
	}

	public void ToggleBackButton(bool show)
	{
		this.buttonBack.SetActive(show);
	}
}
