﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollision : MonoBehaviour
{
	[SerializeField]
	private GameStateReference currentGameState;

	[SerializeField]
	private GameState gameStateDriving;

	[SerializeField]
	private GameEvent onPlayerHitWallEvent;

	private void Start ()
	{
		
	}

	private void Update ()
	{
		
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (currentGameState.currentState == gameStateDriving && collision.gameObject.tag == "Wall")
		{
			onPlayerHitWallEvent.Raise();
		}
	}
}
