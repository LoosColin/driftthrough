﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSystem : MonoBehaviour
{
	[SerializeField]
	private FloatReference gameTimer;

	[SerializeField]
	private FloatReference elapsedStateTime;

	[SerializeField]
	private GameStateReference currentGameState;

	[SerializeField]
	private GameState gameStateReady;

	[SerializeField]
	private GameState gameStateDriving;
	
	private void Start ()
	{
		
	}

	private void Update ()
	{
		elapsedStateTime.variable.value += Time.deltaTime;

		if(currentGameState.currentState == gameStateReady)
		{

		}
		else if(currentGameState.currentState == gameStateDriving)
		{
			gameTimer.variable.value += Time.deltaTime;
		}
	}

	public void OnRetry()
	{
		gameTimer.variable.value = 0f;
	}

	public void OnGameStateChange()
	{
		elapsedStateTime.variable.value = 0f;
		Debug.Log("ELAPSED STATE TIME RESET");
	}
}
