﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateSystem : MonoBehaviour
{
	[SerializeField]
	private FloatReference elapsedStateTime;

	[SerializeField]
	private FloatReference readyCooldown;

	[SerializeField]
	private FloatReference finishCooldown;

	[SerializeField]
	private GameStateReference currentGameState;

	[SerializeField]
	private GameState gameStateReady;

	[SerializeField]
	private GameState gameStateDriving;

	[SerializeField]
	private GameState gameStateFinished;

	[SerializeField]
	private GameEvent onGameStateChangedEvent;

	[SerializeField]
	private GameEvent onBackEvent;

	[SerializeField]
	private GameEvent onGoEvent;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		if(currentGameState.currentState == gameStateReady)
		{
			if (Input.anyKey && elapsedStateTime.value > readyCooldown.value)
			{
				currentGameState.currentState = gameStateDriving;
				onGoEvent.Raise();
				onGameStateChangedEvent.Raise();
			}
		}
		else if(currentGameState.currentState == gameStateDriving)
		{

		}
		else if(currentGameState.currentState == gameStateFinished)
		{
			if(Input.anyKey && elapsedStateTime.value > finishCooldown.value)
			{
				onBackEvent.Raise();
			}
		}
	}

	public void OnRetry()
	{
		currentGameState.currentState = gameStateReady;
		onGameStateChangedEvent.Raise();
	}

	public void OnFinish()
	{
		currentGameState.currentState = gameStateFinished;
		onGameStateChangedEvent.Raise();
	}
}
