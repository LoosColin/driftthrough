﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackSystem : MonoBehaviour
{
	[SerializeField]
	private FloatReference elapsedStateTime;

	[SerializeField]
	private FloatReference backCooldown;

	[SerializeField]
	private GameEvent backEvent;

	public void TryRaiseBack()
	{
		if (elapsedStateTime.value > backCooldown.value)
		{
			backEvent.Raise();
		}
	}

	public void OnBack()
	{
		SceneManager.LoadScene(2);
	}
}
