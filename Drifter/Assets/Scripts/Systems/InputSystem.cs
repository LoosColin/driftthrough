﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour
{
	[SerializeField]
	private GameEvent retry;

	[SerializeField]
	private GameEvent back;

	private void Update ()
	{
		if(Input.GetButton("Retry"))
		{
			retry.Raise();
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			back.Raise();
		}
	}
}
