﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetrySystem : MonoBehaviour
{
	[SerializeField]
	private FloatReference elapsedStateTime;

	[SerializeField]
	private FloatReference retryCooldown;

	[SerializeField]
	private GameEvent retryEvent;

	[SerializeField]
	private GameStateReference currentGameState;

	[SerializeField]
	private GameState gameStateDriving;

	public void TryRaiseRetry()
	{
		if (currentGameState.currentState == gameStateDriving && elapsedStateTime.value > retryCooldown.value)
		{
			retryEvent.Raise();
		}
	}
}
