﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeSystem : MonoBehaviour
{
	[SerializeField]
	private IntReference hits;

	[SerializeField]
	private FloatReference hitCurrentCooldown;

	[SerializeField]
	private FloatReference hitCooldown;

	[SerializeField]
	private FloatReference gameTimer;

	[SerializeField]
	private FloatReference elapsedStateTime;

	[SerializeField]
	private GameStateReference startingGameState;

	[SerializeField]
	private GameStateReference currentGameState;

	private void Start ()
	{
		hits.variable.value = 0;
		hitCurrentCooldown.variable.value = hitCooldown.value;
		gameTimer.variable.value = 0f;
		elapsedStateTime.variable.value = 0f;
		currentGameState.currentState = startingGameState.currentState;
	}
	
	private void Update ()
	{
		
	}
}
