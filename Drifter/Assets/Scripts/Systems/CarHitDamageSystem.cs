﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarHitDamageSystem : MonoBehaviour
{
	[SerializeField]
	private IntReference hits;

	private void Start ()
	{
	}
	
	private void Update ()
	{
		
	}

	public void OnHit()
	{
		hits.variable.value++;
	}

	public void OnRetry()
	{
		hits.variable.value = 0;
	}
}
