﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSoundScript : MonoBehaviour
{
	private AudioSource audioSource;

	private void Start ()
	{
		this.audioSource = this.GetComponent<AudioSource>();

		if (this.audioSource == null)
		{
			Debug.LogError("No hit audiosource attached to car!");
		}

		audioSource.playOnAwake = false;
	}
	
	private void Update ()
	{
		
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Wall")
		{
			audioSource.Play();
		}
	}
}
