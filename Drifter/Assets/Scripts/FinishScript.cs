﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishScript : MonoBehaviour
{
	[SerializeField]
	private GameEvent finishEvent;

	private void Start ()
	{
	}
	
	private void Update ()
	{
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			finishEvent.Raise();
		}
	}
}
