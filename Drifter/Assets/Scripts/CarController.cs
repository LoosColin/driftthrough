﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
	[Header("Car tuning")]
	[SerializeField]
	private float speedForce = 12f;

	[SerializeField]
	private float maxSpeed = 3f;

	[SerializeField]
	private float torqueForce = 235;

	[SerializeField]
	private float driftFactorSticky = 0.9f;

	[SerializeField]
	private float driftFactorSlippy = 1f;

	[SerializeField]
	private float maxStickyVelocity = 0.55f;

	[Header("Tire")]
	[SerializeField]
	private float tireTrackCooldownTime = 0.01f;
	private float tireTrackTime = 0f;

	[SerializeField]
	private GameObject[] wheels;

	[SerializeField]
	private GameObject tireTrackParent;

	[Header("Prefab Links")]
	[SerializeField]
	private GameObject tireTrackPrefab;

	private Rigidbody2D rb;
	private GameplayScript gameplayScript;
	
	private void Start()
	{
		gameplayScript = FindObjectOfType<GameplayScript>();

		if(tireTrackPrefab == null)
		{
			Debug.LogError("TireTrack prefab is not linked!");
		}

		rb = GetComponent<Rigidbody2D>();

		if(tireTrackParent == null)
		{
			GameObject tireTracks = GameObject.Find("TireTracks");

			if (tireTracks == null)
			{
				tireTracks = new GameObject("TireTracks");
			}

			this.tireTrackParent = tireTracks;
		}
	}

	private void Update()
	{
	}

	private void FixedUpdate()
	{
		//Debug.Log("Velocity: " + rb.velocity.magnitude);
		//Debug.Log("Drift: " + RightVelocity().magnitude);

		float driftFactor = driftFactorSticky;

		if(RightVelocity().magnitude > maxStickyVelocity)
		{
			driftFactor = driftFactorSlippy;
		}

		rb.velocity = this.ForwardVelocity() + RightVelocity() * driftFactor;

		if(rb.velocity.magnitude < maxSpeed)
			rb.AddForce(transform.up * speedForce);

		//float tf = Mathf.Lerp(0, torqueForce, rb.velocity.magnitude / 5);

		if (rb.velocity.magnitude > 0.25f)
		{
			float direction = 0f;

			if (Input.touchCount > 0)
			{
				if (Input.GetTouch(0).position.x <= Screen.width / 2)
				{
					direction = 1;
				}
				else
				{
					direction = -1;
				}
			}
			else
			{
				direction = -Input.GetAxis("Horizontal");
			}

			// 1 = left, -1 right
			rb.angularVelocity = direction * torqueForce; // tf usually


		}

		CreateTireTracks();
	}

	private Vector2 ForwardVelocity()
	{
		return transform.up * Vector2.Dot(rb.velocity, transform.up);
	}

	private Vector2 RightVelocity()
	{
		return transform.right * Vector2.Dot(rb.velocity, transform.right);
	}

	private void CreateTireTracks()
	{
		tireTrackTime += Time.deltaTime;

		if (this.RightVelocity().magnitude > 1 && tireTrackTime > tireTrackCooldownTime)
		{
			tireTrackTime = 0f;

			foreach (GameObject wheel in this.wheels)
			{
				Instantiate(this.tireTrackPrefab, wheel.transform.position, wheel.transform.rotation, tireTrackParent.transform);
			}
		}
	}
}